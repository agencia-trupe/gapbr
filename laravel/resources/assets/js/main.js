import AjaxSetup from './AjaxSetup';
import MobileToggle from './MobileToggle';

AjaxSetup();
MobileToggle();

$('select[name=estado]').change(function () {
    let uf = $(this).val(),
        base = $('base').attr('href');

    if (uf) {
        window.location = `${base}/representantes?uf=${uf}`;
    } else {
        window.location = `${base}/representantes`;
    }
});

$('select[name=cidade]').change(function () {
    let uf = $('select[name=estado]').val(),
        cidade = $(this).val(),
        base = $('base').attr('href');

    if (uf && cidade) {
        window.location = `${base}/representantes?uf=${uf}&cidade=${cidade}`;
    } else {
        window.location = `${base}/representantes`;
    }
});

$('#form-newsletter').submit(function (event) {
    event.preventDefault();

    const $form = $(this);

    if ($form.hasClass('sending')) return false;

    $form.addClass('sending');

    $.ajax({
        type: 'POST',
        url: `${$('base').attr('href')}/newsletter`,
        data: {
            nome: $form.find('input[name=n_nome]').val(),
            email: $form.find('input[name=n_email]').val(),
            segmento: $form.find('select[name=n_segmento] option:selected').val(),
        },
    })
        .done((data) => {
            alert(data.message);
            $form[0].reset();
        })
        .fail((data) => {
            if (data.status !== 422) {
                console.log(data);
                return;
            }
            let res = data.responseJSON,
                txt = [],
                field = '';
            for (field in res) {
                res[field].map(error => txt.push(error));
            }
            alert(txt.join('\n'));
        })
        .always(() => {
            $form.removeClass('sending');
        });
});

$('.banners').cycle({
    slides: '>.banner-slide',
});

$('.header-busca').click((event) => {
    event.preventDefault();

    const $modal = $('.busca-modal');
    const hideModal = function (event) {
        if (
            !$('.header-busca').is(event.target) &&
            !$modal.is(event.target) &&
            $('.select2-container').has(event.target).length === 0 &&
            $modal.has(event.target).length === 0
        ) {
            $modal.removeClass('open');
            $(window).off('click', hideModal);
        }
    };

    $modal.addClass('open');
    $(window).on('click', hideModal);
});

const select2options = {
    theme: 'gapbr',
    language: {
        noResults() {
            return 'Nenhum resultado encontrado.';
        },
    },
};

$('.busca-avancada select').select2(select2options);

const selectTodosModelos = $('.busca-avancada select[name=modelo]').html();

$('.busca-avancada select[name=marca]')
    .change(function () {
        const selecao = $(this).val();
        $('.busca-avancada select[name=modelo]').html(selectTodosModelos);
        $('.busca-avancada select[name=modelo] option').each((idx, el) => {
            if (selecao && $(el).data('montadora') !== selecao && $(el).val() !== '') {
                $(el).remove();
            }
        });
        $('.busca-avancada select[name=modelo]').select2('destroy');
        $('.busca-avancada select[name=modelo]').select2(select2options);
    })
    .trigger('change');

$('.novidades-ver-mais').click(function (event) {
    event.preventDefault();

    let $btn = $(this),
        $container = $('.novidades-posts');

    if ($btn.hasClass('loading')) return false;

    $btn.addClass('loading');

    $.get($btn.data('next'), (data) => {
        const posts = $(data.posts).hide();
        $container.append(posts);
        posts.fadeIn();

        $btn.removeClass('loading');

        if (data.nextPage) {
            $btn.data('next', data.nextPage);
        } else {
            $btn.fadeOut(() => {
                $this.remove();
            });
        }
    });
});
