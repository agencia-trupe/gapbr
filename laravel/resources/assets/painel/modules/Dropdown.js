export default function Dropdown() {
    $('.dropdown-filterable').select2({
        theme: 'bootstrap',
        language: {
            noResults: function() {
                return 'Nenhum resultado encontrado.';
            }
        }
    });

    $('.dropdown-tags').select2({
        theme: 'bootstrap',
        tags: true
    });
}
