@extends('frontend.common.template')

@section('content')

    <div class="main produtos">
        <div class="center">
            <h1>Produtos GAP<span>br</span></h1>
            <div class="texto">{!! $produtosTexto->texto !!}</div>

            <div class="produtos-links">
                @foreach($categorias as $cat)
                <a href="{{ route('produtos', $cat->slug) }}">
                    <img src="{{ asset('assets/img/produtos/categorias/'.$cat->imagem) }}" alt="">
                    <h3>{{ $cat->titulo }}</h3>
                    <p>{{ $cat->chamada }}</p>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
