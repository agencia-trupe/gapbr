@extends('frontend.common.template')

@section('content')

    <div class="main produtos-categoria">
        <div class="center">
            <img src="{{ asset('assets/img/produtos/categorias/'.$categoria->imagem) }}" alt="" class="imagem">

            <div class="texto">
                <h1>{{ $categoria->titulo }}</h1>
                {!! $categoria->texto !!}

                <div class="chamada-catalogo">
                    <p>
                        Consulte todos os(as) <span>{{ $categoria->titulo }}</span> em nosso Catálogo Online:
                    </p>
                    <a href="{{ route('catalogo', ['linha' => $categoria->id]) }}">
                        <span>Buscar {{ $categoria->titulo }}</span>
                    </a>
                </div>
            </div>

            <div class="divider"></div>

            <div class="produtos-links">
                @foreach($categorias as $cat)
                @if($cat->id != $categoria->id)
                <a href="{{ route('produtos', $cat->slug) }}">
                    <img src="{{ asset('assets/img/produtos/categorias/'.$cat->imagem) }}" alt="">
                    <h3>{{ $cat->titulo }}</h3>
                </a>
                @endif
                @endforeach
            </div>
        </div>
    </div>

@endsection
