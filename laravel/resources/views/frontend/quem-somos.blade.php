@extends('frontend.common.template')

@section('content')

    <div class="main quem-somos">
        <div class="center">
            <h1>Quem Somos</h1>
            <div class="texto">{!! $quemSomos->texto !!}</div>

            <div class="quem-somos-textos">
                @foreach($textos as $texto)
                <div>
                    <img src="{{ asset('assets/img/quem-somos/'.$texto->imagem) }}" alt="">
                    <h3>{{ $texto->titulo }}</h3>
                    <p>{{ $texto->texto }}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
