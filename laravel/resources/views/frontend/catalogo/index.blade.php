@extends('frontend.common.template')

@section('content')

    <div class="catalogo">
        @include('frontend.common.busca-avancada')

        <div class="main">
            <div class="center">
                @if(count($produtos))
                    @if(isset($busca))
                    <h1>Resultado da Busca</h1>
                    @endif
                    <div class="produtos-thumbs">
                        @foreach($produtos as $produto)
                        <a href="{{ route('catalogo.show', $produto->slug) }}">
                            <div class="imagem">
                                <img src="{{ asset('assets/img/produtos/thumbs/'.$produto->imagem) }}" alt="">
                            </div>
                            <span>{{ $produto->titulo }}</span>
                        </a>
                        @endforeach
                    </div>
                    @if(! isset($busca))
                    {!! $produtos->render() !!}
                    @endif
                @else
                <div class="nenhum">Nenhum produto encontrado.</div>
                @endif
            </div>
        </div>
    </div>

@endsection
