@extends('frontend.common.template')

@section('content')

    <div class="catalogo-pdf">
        <div class="bg">
            <div class="center">
                <h1>Catálogo PDF</h1>
                <div class="texto">{!! $catalogoPdf->texto !!}</div>
            </div>
        </div>

        @if($catalogoPdf->arquivo)
            <a href="{{ asset('assets/catalogo-pdf/'.$catalogoPdf->arquivo) }}" target="_blank" class="download"></a>
        @endif
    </div>

@endsection
