@extends('frontend.common.template')

@section('content')

    <div class="catalogo">
        @include('frontend.common.busca-avancada')

        <div class="main">
            <div class="center">
                <h1>{{ $produto->titulo }}</h1>
                <div class="catalogo-produto">
                    <div class="imagem">
                        <img src="{{ asset('assets/img/produtos/'.$produto->imagem) }}" alt="">
                    </div>
                    <div class="detalhes">
                        <h3>{{ $produto->categoria->titulo }}</h3>
                        <p style="margin-top:5px">
                            CÓDIGO GAP {{ $produto->codigo_gap }}
                        </p>
                        <p style="margin-top:3px">
                            NÚMERO ORIGINAL<br>{!! $produto->numero_original !!}
                        </p>
                        <p class="dimensoes">
                            <strong>DIMENSÕES</strong><br>
                            {!! $produto->dimensoes !!}
                        </p>

                        @if(count($produto->veiculos))
                        <div class="veiculos">
                            <table>
                                <thead>
                                    <tr>
                                        <th>Montadora/Veículo</th>
                                        <th>Ano</th>
                                        <th>Posição</th>
                                        <th>DHM</th>
                                        <th>Lado</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($produto->veiculos as $v)
                                    <tr>
                                        <td>
                                            {{ $v->montadora }} / {{ $v->veiculo }}
                                        </td>
                                        <td>{{ $v->ano }}</td>
                                        <td>{{ $v->posicao }}</td>
                                        <td>{{ $v->dhm }}</td>
                                        <td>{{ $v->lado }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif

                        @if($produto->observacoes)
                        <p class="observacoes">
                            <em>Observações:</em><br>
                            {!! $produto->observacoes !!}
                        </p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
