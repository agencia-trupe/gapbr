@extends('frontend.common.template')

@section('content')

    <div class="home">
        <div class="banners">
            @foreach($banners as $banner)
            <div class="banner-slide" style="background-image:url({{ asset('assets/img/banners/'.$banner->background) }}">
                <div class="center">
                    <div class="banner-texto banner-{{ $banner->cor }}">
                        <p>{!! $banner->texto !!}</p>
                        @if($banner->link && $banner->botao)
                        <a href="{{ Tools::parseLink($banner->link) }}">
                            {{ $banner->botao }}
                        </a>
                        @endif
                    </div>
                    <div class="banner-imagens">
                        @foreach(range(1, 3) as $i)
                        @if($banner->{'imagem_'.$i})
                        <div class="imagem">
                            <img src="{{ asset('assets/img/banners/'.$banner->{'imagem_'.$i}) }}" alt="">
                        </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            </div>
            @endforeach
            <div class="cycle-arrow cycle-prev">&larr;</div>
            <div class="cycle-arrow cycle-next">&rarr;</div>
            <div class="cycle-pager"></div>
        </div>
        @include('frontend.common.busca-avancada')
        <div class="home-produtos">
            <div class="center">
                <div class="texto">
                    <h2>Nossos Produtos</h2>
                    <p>Conheça todos os produtos:</p>
                    <a href="{{ route('produtos') }}"><span>Buscar Linha Completa</span></a>
                </div>
                <div class="links @if(count($categorias) == 4) links-col-4 @endif">
                    @foreach($categorias as $categoria)
                    <a href="{{ route('produtos', $categoria->slug) }}">
                        <img src="{{ asset('assets/img/produtos/categorias/home/'.$categoria->imagem_home) }}" alt="">
                        <p>
                            <span>{{ $categoria->titulo }}</span>
                        </p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="home-chamadas">
            <div class="center">
                <img src="{{ asset('assets/img/home/'.$home->imagem) }}" alt="">
                <div class="chamadas">
                    @foreach(range(1, 4) as $i)
                    <div>
                        <span>0{{ $i }}</span>
                        <p>{{ $home->{'chamada_'.$i} }}</p>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="home-novidades">
            <div class="center">
                <div class="texto">
                    <h2>Novidades</h2>
                    <p>Acompanhe todos os nossos lançamentos, atualizações e dicas reunidas aqui.</p>
                </div>
                <div class="links">
                    @foreach($novidades as $novidade)
                    <a href="{{ route('novidades.show', [$novidade->categoria->slug, $novidade->slug]) }}">
                        <img src="{{ asset('assets/img/novidades/'.$novidade->capa) }}" alt="">
                        <p>
                            <span>{{ $novidade->titulo }}</span>
                        </p>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="newsletter">
            <div class="center">
                <form action="#" id="form-newsletter">
                    <p>
                        <span>Receba</span>
                        nossas novidades
                    </p>
                    <input type="text" name="n_nome" placeholder="nome" required>
                    <input type="email" name="n_email" placeholder="e-mail" required>
                    <select name="n_segmento" required>
                        <option value="">segmento</option>
                        @foreach(Tools::segmentosNewsletter() as $s)
                        <option value="{{ $s }}">{{ $s }}</option>
                        @endforeach
                    </select>
                    <input type="submit" value="Cadastrar">
                </form>
            </div>
        </div>
    </div>

@endsection
