<aside>
    <form action="{{ route('novidades') }}" method="GET" class="busca">
        <input type="text" name="busca" placeholder="buscar" value="{{ request()->get('busca') }}" required>
        <input type="submit" value="">
    </form>

    <nav>
        @foreach($categorias as $cat)
        <a href="{{ route('novidades', $cat->slug) }}" @if(isset($categoria) && $categoria == $cat) class="active" @endif>{{ $cat->titulo }}</a>
        @endforeach
    </nav>

    <form action="#" id="form-newsletter">
        <p>
            <span>Receba</span>
            nossas novidades
        </p>
        <div class="inputs">
            <input type="text" name="n_nome" placeholder="nome" required>
            <input type="email" name="n_email" placeholder="e-mail" required>
            <select name="n_segmento" required>
                <option value="">segmento</option>
                @foreach(Tools::segmentosNewsletter() as $s)
                <option value="{{ $s }}">{{ $s }}</option>
                @endforeach
            </select>
        </div>
        <input type="submit" value="Cadastrar">
    </form>
</aside>
