<a href="{{ route('novidades.show', [$post->categoria->slug, $post->slug]) }}" class="thumb">
    <div class="imagem">
        <img src="{{ asset('assets/img/novidades/thumb-sm/'.$post->capa) }}" alt="">
        <span class="data">{{ Tools::formataData($post->data) }}</span>
    </div>
    <div class="thumb-texto">
        <span class="categoria">{{ $post->categoria->titulo }}</span>
        <div class="titulo">
            <h5>{{ $post->titulo }}</h5>
            <span class="mais">LER MAIS</span>
        </div>
    </div>
</a>
