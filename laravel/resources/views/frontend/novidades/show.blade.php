@extends('frontend.common.template')

@section('content')

    <div class="novidades novidades-show">
        <div class="capa">
            <div class="center">
                <div class="titulo-wrapper">
                    <h1>Novidades</h1>
                    <div class="titulo">
                        <span class="categoria">
                            {{ $post->categoria->titulo }}
                        </span>
                        <h2>{{ $post->titulo }}</h2>
                    </div>
                </div>

                <div class="imagem">
                    <img src="{{ asset('assets/img/novidades/'.$post->capa) }}" alt="">
                    <span>{{ Tools::formataData($post->data) }}</span>
                </div>
            </div>
        </div>

        <div class="center">
            <article>
                <div class="texto">
                    {!! $post->texto !!}
                </div>
                <div class="voltar">
                    <a href="{{ route('novidades') }}">voltar</a>
                </div>
            </article>
            @include('frontend.novidades._aside')
        </div>
    </div>

@endsection
