@extends('frontend.common.template')

@section('content')

    <div class="novidades novidades-index">
        @if(count($posts) && !request()->get('busca'))
        <a href="{{ route('novidades.show', [$posts->first()->categoria->slug, $posts->first()->slug]) }}" class="capa">
            <div class="center">
                <div class="titulo-wrapper">
                    <h1>Novidades</h1>
                    <div class="titulo">
                        <span class="categoria">{{ $posts->first()->categoria->titulo }}</span>
                        <h2>{{ $posts->first()->titulo }}</h2>
                        <span class="mais">LER MAIS</span>
                    </div>
                </div>

                <div class="imagem">
                    <img src="{{ asset('assets/img/novidades/'.$posts->first()->capa) }}" alt="">
                    <span>{{ Tools::formataData($posts->first()->data) }}</span>
                </div>
            </div>
        </a>
        @else
        <div class="capa">
            <div class="center">
                <div class="titulo-wrapper">
                    <h1>Novidades</h1>
                </div>
            </div>
        </div>
        @endif

        <?php $limite = request()->get('busca') ? 3 : 4; ?>
        <div class="center">
            <div class="novidades-posts-wrapper">
                <div class="novidades-posts">
                @if(!count($posts))
                    <div class="nenhum">Nenhuma postagem encontrada.</div>
                @else
                    @foreach($posts as $key => $post)
                        @if(!$key == 0 || request()->get('busca'))
                            @if($key <= $limite)
                            <a href="{{ route('novidades.show', [$post->categoria->slug, $post->slug]) }}" class="top4">
                                <div class="imagem-wrapper">
                                    <div class="imagem">
                                        <img src="{{ asset('assets/img/novidades/thumb-md/'.$post->capa) }}" alt="">
                                        <span class="categoria">{{ $post->categoria->titulo }}</span>
                                        <span class="data">{{ Tools::formataData($post->data) }}</span>
                                    </div>
                                </div>
                                <div class="titulo">
                                    <h5>{{ $post->titulo }}</h5>
                                    <span class="mais">LER MAIS</span>
                                </div>
                            </a>
                            @else
                            @include('frontend.novidades._thumb', ['post' => $post])
                            @endif
                        @endif
                    @endforeach
                @endif
                </div>

                @if ($posts->hasMorePages())
                <a href="#" class="novidades-ver-mais" data-next="{{ route('novidades', [null, 'page' => 2, 'busca' => request()->get('busca')]) }}">
                    <div>carregar mais</div>
                </a>
                @endif
            </div>

            @include('frontend.novidades._aside')
        </div>
    </div>

@endsection
