@extends('frontend.common.template')

@section('content')

    <div class="main qualidade">
        <div class="bg">
            <div class="center">
                <div class="texto texto-1">
                    <h1>Qualidade</h1>
                    {!! $qualidade->texto_1 !!}
                </div>
                @if($qualidade->imagem)
                <img src="{{ asset('assets/img/qualidade/'.$qualidade->imagem) }}" alt="">
                @endif
            </div>
        </div>

        <div class="center">
            <div class="certificacoes-manual">
                @if($certificacoes->count())
                <div class="certificacoes">
                    @foreach($certificacoes as $c)
                    <img src="{{ asset('assets/img/certificacoes/'.$c->imagem) }}" alt="">
                    @endforeach
                </div>
                @endif

                @if($qualidade->manual_arquivo)
                <a href="{{ asset('assets/manual-de-qualidade/'.$qualidade->manual_arquivo) }}" class="manual" target="_blank">
                    <img src="{{ asset('assets/img/qualidade/'.$qualidade->manual_capa) }}" alt="">
                    <span class="pdf"></span>
                    <div class="manual-texto">
                        <span>Faça o download do nosso</span>
                        MANUAL DE QUALIDADE
                    </div>
                </a>
                @endif
            </div>

            <div class="texto texto-2">
                {!! $qualidade->texto_2 !!}
            </div>
        </div>
    </div>

@endsection
