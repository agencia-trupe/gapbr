<div class="busca-modal">
    <div class="busca-avancada busca-conteudo">
        <div class="center">
            <div class="box">
                <h2>Busca de Conteúdo</h2>
                <form action="{{ route('busca') }}">
                    <input type="text" name="palavra_chave" placeholder="palavra-chave" value="{{ request('palavra_chave') }}" required>
                    <input type="submit">
                </form>
            </div>
        </div>
    </div>
    @include('frontend.common.busca-avancada', ['title' => 'Busca de Produtos'])
</div>
