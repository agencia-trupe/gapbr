<div class="busca-avancada">
    <div class="center">
        <div class="box">
            <h2>{{ $title or 'Busca Avançada' }}</h2>
            <div class="row">
                <h3>Por Código</h3>
                <form action="{{ route('catalogo') }}">
                    <input type="text" name="codigo_gap" placeholder="código GAP" value="{{ request('codigo_gap') }}">
                    <input type="text" name="numero_original" placeholder="número original" value="{{ request('numero_original') }}">
                    <input type="text" name="equivalencia" placeholder="tabela de equivalência" value="{{ request('equivalencia') }}">
                    <input type="submit">
                </form>
            </div>
            <div class="row">
                <h3>Por Aplicação / Tipo de Produto</h3>
                <form action="{{ route('catalogo') }}">
                    <label>Veículo:</label>
                    <select name="marca" name="marca">
                        <option value="">marca</option>
                        @foreach($buscaMarcas as $marca)
                        <option value="{{ $marca }}" @if(request('marca') == $marca) selected @endif>{{ $marca }}</option>
                        @endforeach
                    </select>
                    <select name="modelo">
                        <option value="">modelo</option>
                        @foreach($buscaModelos as $veiculo)
                        <option value="{{ $veiculo->veiculo }}" @if(request('modelo') == $veiculo->veiculo) selected @endif data-montadora="{{ $veiculo->montadora }}">{{ $veiculo->veiculo }}</option>
                        @endforeach
                    </select>
                    <select name="ano">
                        <option value="">ano</option>
                        @foreach($buscaAnos as $ano)
                        <option value="{{ $ano }}" @if(request('ano') == $ano) selected @endif>{{ $ano }}</option>
                        @endforeach
                    </select>
                    <hr>
                    <label>Tipo:</label>
                    <select name="linha">
                        <option value="">linha de produto</option>
                        @foreach($buscaLinhas as $id => $linha)
                        <option value="{{ $id }}" @if(request('linha') == $id) selected @endif>{{ $linha }}</option>
                        @endforeach
                    </select>
                    <input type="submit">
                </form>
            </div>
        </div>
    </div>
</div>
