<a href="{{ route('quemSomos') }}" @if(Tools::routeIs('quemSomos')) class="active" @endif>Quem Somos</a>
<a href="{{ route('produtos') }}" @if(Tools::routeIs('produtos')) class="active" @endif>Produtos</a>
<div class="dropdown">
    <span @if(Tools::routeIs('catalogo*')) class="active" @endif>Catálogo</span>
    <div class="dropdown-items">
        <a href="{{ route('catalogo') }}" @if(Tools::routeIs('catalogo')) class="active" @endif>Catálogo Online</a>
        <a href="{{ route('catalogoPdf') }}" @if(Tools::routeIs('catalogoPdf')) class="active" @endif>Catálogo PDF</a>
    </div>
</div>
<a href="{{ route('novidades') }}" @if(Tools::routeIs('novidades*')) class="active" @endif>Novidades</a>
<a href="{{ route('qualidade') }}" @if(Tools::routeIs('qualidade')) class="active" @endif>Qualidade</a>
<a href="{{ route('representantes') }}" @if(Tools::routeIs('representantes')) class="active" @endif>Representantes</a>
<a href="{{ route('contato') }}" @if(Tools::routeIs('contato')) class="active" @endif>Contato</a>
