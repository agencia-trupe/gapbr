    <footer>
        <div class="center">
            <div class="links">
                <a href="{{ route('home') }}">Home</a>
                <a href="{{ route('quemSomos') }}">Quem Somos</a>
                <a href="{{ route('produtos') }}">Produtos</a>
                <a href="{{ route('novidades') }}">Novidades</a>
            </div>
            <div class="links">
                <a href="{{ route('qualidade') }}">Qualidade</a>
                <a href="{{ route('representantes') }}">Representantes</a>
                <a href="{{ route('contato') }}">Contato</a>
            </div>
            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="endereco">{!! $contato->endereco !!}</p>
                <div class="social">
                    @foreach(['facebook', 'instagram'] as $s)
                        <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                    @endforeach
                </div>
            </div>
            <div class="copyright">
                <img src="{{ asset('assets/img/layout/marca-gapbr-rodape.png') }}" alt="">
                <p>
                    © {{ date('Y') }} {{ config('app.name') }} General Auto Parts - Todos os direitos reservados.<br>
                    <a href="https://www.trupe.net" target="_blank">Criação de Sites</a>: <a href="https://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
