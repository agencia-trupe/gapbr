    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">
                <img src="{{ asset('assets/img/layout/marca-gapbr.png') }}" alt="">
            </a>
            <div class="header-contatos">
                <p class="telefone">{{ $contato->telefone }}</p>
                <div class="social">
                    @foreach(['facebook', 'instagram'] as $s)
                        <a href="{{ $contato->{$s} }}" class="{{ $s }}" target="_blank">{{ $s }}</a>
                    @endforeach
                </div>
            </div>
            <nav id="nav-desktop">
                @include('frontend.common.nav')
                <a href="#" class="header-busca">busca</a>
            </nav>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common.nav')
        </nav>
    </header>
