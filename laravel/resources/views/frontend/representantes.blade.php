@extends('frontend.common.template')

@section('content')

    <div class="main representantes">
        <div class="bg"></div>
        <div class="center">
            <div class="texto">
                <h1>Encontre o seu Representante</h1>
                <p>Encontre o distribuidor GAP mais perto de você.</p>

                {!! Form::select('estado', array_combine($estados, $estados), request('uf'), ['placeholder' => 'Estado']) !!}
                @if(isset($cidades) && count($cidades))
                {!! Form::select('cidade', array_combine($cidades, $cidades), request('cidade'), ['placeholder' => 'Cidade']) !!}
                @else
                {!! Form::select('cidade', [], request('cidade'), ['placeholder' => 'Cidade', 'disabled' => true]) !!}
                @endif

                <div class="representantes-lista">
                    @if(isset($representantes))
                        @if(count($representantes))
                        @foreach($representantes as $representante)
                        <div class="representante">
                            <span class="titulo">{{ $representante->titulo }}</span>

                            @foreach($representante->cadastros as $cadastro)
                            <div class="cadastro">
                                <span>{{ $cadastro->nome }}</span>
                                @foreach(explode(',', $cadastro->telefones) as $telefone)
                                <span class="telefone">{{ trim($telefone) }}</span>
                                @endforeach
                                <a href="mailto:{{ $cadastro->e_mail }}" class="email">
                                    {{ $cadastro->e_mail }}
                                </a>
                            </div>
                            @endforeach

                            <span>{{ $representante->cidade }} - {{ $representante->estado }}</span>
                        </div>
                        @endforeach
                        @else
                        <div class="nenhum">
                            Não há representantes nesse estado.<br>Por favor, selecione um estado vizinho.
                        </div>
                        @endif
                    @endif
                </div>
            </div>
            <div class="mapa">
                @foreach(Tools::listaEstados() as $uf)
                <a href="{{ route('representantes', ['uf' => $uf]) }}" class="{{ strtolower($uf) }} @if(request('uf') == $uf) active @endif" title="{{ $uf }}">{{ $uf }}</a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
