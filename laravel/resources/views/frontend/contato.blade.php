@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="bg">
            <div class="center">
                <div class="texto">
                    <h1>Fale Conosco</h1>
                    <p class="telefone">{{ $contato->telefone }}</p>
                    <p class="endereco">{!! $contato->endereco !!}</p>
                    <p class="atendimento">
                        <span>atendimento</span>
                        {{ $contato->atendimento }}
                    </p>
                </div>

                <form action="{{ route('contato.post') }}" method="POST">
                    @if($errors->any())
                    <div class="erro">
                        @foreach($errors->all() as $error)
                        {!! $error !!}<br>
                        @endforeach
                    </div>
                    @endif
                    @if(session('enviado'))
                    <div class="enviado">
                        Mensagem enviada com sucesso!
                    </div>
                    @endif

                    {!! csrf_field() !!}

                    <div class="col">
                        <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                        <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                        <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                        <input type="text" name="cidade_uf" placeholder="cidade/UF" value="{{ old('cidade_uf') }}">
                    </div>
                    <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="Enviar">
                </form>
            </div>
        </div>

        <div class="center">
            <div class="imagem">
                <img src="{{ asset('assets/img/contato/'.$contato->imagem) }}" alt="">
            </div>
        </div>
    </div>

@endsection
