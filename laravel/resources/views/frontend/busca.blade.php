@extends('frontend.common.template')

@section('content')

    <div class="main busca">
        <div class="center">
            <h1>
                Resultados da Busca
                <small>({{ request('palavra_chave') }})</small>
            </h1>

            <div class="busca-resultados">
            @if(count($resultados))
                @foreach($resultados as $r)
                    <a href="{{ $r['link'] }}">
                        @if($r['secao'])
                        <span class="secao">{{ $r['secao'] }}</span>
                        @endif
                        <span class="titulo">{{ $r['titulo'] }}</span>
                    </a>
                @endforeach
            @else
                <div class="nenhum">Nenhum resultado encontrado.</div>
            @endif
            </div>
        </div>
    </div>

@endsection
