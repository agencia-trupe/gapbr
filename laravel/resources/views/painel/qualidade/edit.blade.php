@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Qualidade
            <a href="{{ route('painel.certificacoes.index') }}" class="btn btn-sm btn-warning pull-right">
                <span class="glyphicon glyphicon-star-empty" style="margin-right:10px"></span>
                Certificações
            </a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.qualidade.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.qualidade.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
