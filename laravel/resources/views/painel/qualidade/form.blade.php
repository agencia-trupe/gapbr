@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('texto_1', 'Texto 1') !!}
    {!! Form::textarea('texto_1', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_2', 'Texto 2') !!}
    {!! Form::textarea('texto_2', null, ['class' => 'form-control ckeditor', 'data-editor' => 'qualidade']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/qualidade/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="well form-group">
    {!! Form::label('manual_arquivo', 'Manual Arquivo') !!}
    @if($registro->manual_arquivo)
    <a href="{{ asset('assets/manual-de-qualidade/'.$registro->manual_arquivo) }}" style="display:block;margin-bottom:10px" target="_blank">{{ $registro->manual_arquivo }}</a>
    @endif
    {!! Form::file('manual_arquivo', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('manual_capa', 'Manual Capa') !!}
    @if($registro->manual_capa)
    <img src="{{ url('assets/img/qualidade/'.$registro->manual_capa) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('manual_capa', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
