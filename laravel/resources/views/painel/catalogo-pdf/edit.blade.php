@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            <small>Produtos /</small> Catálogo PDF
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.catalogo-pdf.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.catalogo-pdf.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
