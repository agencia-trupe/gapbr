@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos-texto.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos-texto.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
