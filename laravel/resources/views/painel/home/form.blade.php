@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/home/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_1', 'Chamada 1') !!}
    {!! Form::text('chamada_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_2', 'Chamada 2') !!}
    {!! Form::text('chamada_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_3', 'Chamada 3') !!}
    {!! Form::text('chamada_3', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada_4', 'Chamada 4') !!}
    {!! Form::text('chamada_4', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
