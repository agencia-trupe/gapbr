@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('background', 'Background') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->background) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('background', ['class' => 'form-control']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('cor', 'Cor do Texto') !!}
    {!! Form::select('cor', [
        'branco' => 'Branco',
        'preto'  => 'Azul'
    ], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('botao', 'Botão') !!}
            {!! Form::text('botao', null, ['class' => 'form-control']) !!}
        </div>

    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('link', 'Link') !!}
            {!! Form::text('link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
@foreach(range(1, 3) as $i)
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_'.$i, 'Imagem '.$i) !!}
        @if($submitText == 'Alterar' && $registro->{'imagem_'.$i})
            <div>
                <a href="{{ route('painel.banners.removerImagem', [$registro->id, $i]) }}" class="btn btn-xs btn-danger">
                    <span class="glyphicon glyphicon-trash" style="margin-right:5px"></span>
                    remover
                </a>
            </div>
            <img src="{{ url('assets/img/banners/'.$registro->{'imagem_'.$i}) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('imagem_'.$i, ['class' => 'form-control']) !!}
        </div>
    </div>
@endforeach
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
