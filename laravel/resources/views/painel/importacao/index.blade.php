@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Importação</h2>
    </legend>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Importar Produtos</h4>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => 'painel.importacao.produtos', 'files' => true]) !!}
                <div class="input-group">
                    {!! Form::file('arquivo', ['class' => 'form-control', 'required' => true]) !!}
                    <span class="input-group-btn">
                        {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
                    </span>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Importar Veículos</h4>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => 'painel.importacao.veiculos', 'files' => true]) !!}
                <div class="input-group">
                    {!! Form::file('arquivo', ['class' => 'form-control', 'required' => true]) !!}
                    <span class="input-group-btn">
                        {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
                    </span>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Importar Equivalências</h4>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => 'painel.importacao.equivalencia', 'files' => true]) !!}
                <div class="input-group">
                    {!! Form::file('arquivo', ['class' => 'form-control', 'required' => true]) !!}
                    <span class="input-group-btn">
                        {!! Form::submit('Enviar', ['class' => 'btn btn-success']) !!}
                    </span>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Importar Imagens</h4>
        </div>
        <div class="panel-body">
            <a href="{{ route('painel.importacao.imagens') }}" class="btn btn-info btn-block">
                Importar Imagens
            </a>
        </div>
    </div>

@endsection
