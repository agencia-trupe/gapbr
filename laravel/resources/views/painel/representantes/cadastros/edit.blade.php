@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Representantes / {{ $representante->titulo }} / Cadastros /</small> Editar Cadastro</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.representantes.cadastros.update', $representante->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.representantes.cadastros.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
