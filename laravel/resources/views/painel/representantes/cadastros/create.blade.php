@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Representantes / {{ $representante->titulo }} / Cadastros /</small> Adicionar Cadastro</h2>
    </legend>

    {!! Form::open(['route' => ['painel.representantes.cadastros.store', $representante->id], 'files' => true]) !!}

        @include('painel.representantes.cadastros.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
