@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('estado', 'Estado') !!}
    {!! Form::select('estado', Tools::listaEstados(), null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade', 'Cidade') !!}
    {!! Form::text('cidade', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.representantes.index') }}" class="btn btn-default btn-voltar">Voltar</a>
