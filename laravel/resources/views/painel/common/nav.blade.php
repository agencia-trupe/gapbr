<ul class="nav navbar-nav">
	<li @if(Tools::routeIs('painel.home*')) class="active" @endif>
		<a href="{{ route('painel.home.index') }}">Home</a>
	</li>
	<li @if(Tools::routeIs('painel.banners*')) class="active" @endif>
		<a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(Tools::routeIs('painel.quem-somos*')) class="active" @endif>
        <a href="{{ route('painel.quem-somos.index') }}">Quem Somos</a>
    </li>
    <li class="dropdown @if(Tools::routeIs(['painel.produtos*', 'painel.catalogo-pdf*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Produtos
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.produtos.*') && !Tools::routeIs('painel.produtos.equivalencia*')) class="active" @endif>
                <a href="{{ route('painel.produtos.index') }}">Produtos</a>
            </li>
            <li @if(Tools::routeIs('painel.produtos-texto*')) class="active" @endif>
                <a href="{{ route('painel.produtos-texto.index') }}">Texto</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.produtos.equivalencia*')) class="active" @endif>
                <a href="{{ route('painel.produtos.equivalencia.index') }}">Tabela de Equivalência</a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.catalogo-pdf*')) class="active" @endif>
                <a href="{{ route('painel.catalogo-pdf.index') }}">Catálogo PDF</a>
            </li>
        </ul>
    </li>
    <li @if(Tools::routeIs('painel.novidades*')) class="active" @endif>
        <a href="{{ route('painel.novidades.index') }}">Novidades</a>
    </li>
	<li @if(Tools::routeIs(['painel.qualidade*', 'painel.certificacoes*'])) class="active" @endif>
		<a href="{{ route('painel.qualidade.index') }}">Qualidade</a>
	</li>
	<li @if(Tools::routeIs('painel.representantes*')) class="active" @endif>
		<a href="{{ route('painel.representantes.index') }}">Representantes</a>
	</li>
    <li class="dropdown @if(Tools::routeIs(['painel.contato*', 'painel.newsletter*'])) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li @if(Tools::routeIs('painel.contato.index')) class="active" @endif>
                <a href="{{ route('painel.contato.index') }}">Informações de Contato</a>
            </li>
            <li @if(Tools::routeIs('painel.contato.recebidos*')) class="active" @endif>
                <a href="{{ route('painel.contato.recebidos.index') }}">
                    Contatos Recebidos
                    @if($contatosNaoLidos >= 1)
                    <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                    @endif
                </a>
            </li>
            <li class="divider"></li>
            <li @if(Tools::routeIs('painel.newsletter*')) class="active" @endif>
                <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
            </li>
        </ul>
    </li>
</ul>
