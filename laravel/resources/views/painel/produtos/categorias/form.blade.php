@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::text('titulo', null, ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/categorias/'.$categoria->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem_home', 'Imagem Home') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/produtos/categorias/home/'.$categoria->imagem_home) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem_home', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('chamada', 'Chamada') !!}
    {!! Form::text('chamada', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'produtoCategoria']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.categorias.index') }}" class="btn btn-default btn-voltar">Voltar</a>
