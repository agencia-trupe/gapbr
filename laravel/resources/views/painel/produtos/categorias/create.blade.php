@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos /</small> Adicionar Categoria</h2>
    </legend>

    {!! Form::open([
        'route' => 'painel.produtos.categorias.store',
        'files' => true
    ]) !!}

        @include('painel.produtos.categorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
