@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo }} /</small> Adicionar Veículo</h2>
    </legend>

    {!! Form::open([
        'route' => ['painel.produtos.veiculos.store', $produto->id],
        'files' => true
    ]) !!}

        @include('painel.produtos.veiculos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
