@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.produtos.index') }}" title="Voltar para Produtos" class="btn btn-sm btn-default">
        &larr; Voltar para Produtos    </a>

    <legend>
        <h2>
            <small>Produtos / {{ $produto->titulo }} /</small> Veículos

            <a href="{{ route('painel.produtos.veiculos.create', $produto->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Veículo</a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="produtos_veiculos">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Montadora / Veículo</th>
                <th>Ano</th>
                <th>Posição</th>
                <th>DHM</th>
                <th>Lado</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>
                    {{ $registro->montadora }} / {{ $registro->veiculo }}
                </td>
                <td>{{ $registro->ano }}</td>
                <td>{{ $registro->posicao }}</td>
                <td>{{ $registro->dhm }}</td>
                <td>{{ $registro->lado }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.produtos.veiculos.destroy', $produto->id, $registro->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.produtos.veiculos.edit', [$produto->id, $registro->id]) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
