@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / {{ $produto->titulo }} /</small> Editar Veículo</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.produtos.veiculos.update', $produto->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.produtos.veiculos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
