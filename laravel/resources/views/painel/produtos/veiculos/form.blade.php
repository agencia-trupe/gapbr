@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('montadora', 'Montadora') !!}
    {!! Form::select('montadora', $montadoras, null, ['class' => 'form-control dropdown-tags', 'placeholder' => 'Selecione ou digite e aperte [Enter]']) !!}
</div>

<div class="form-group">
    {!! Form::label('veiculo', 'Veículo') !!}
    {!! Form::select('veiculo', $veiculos, null, ['class' => 'form-control dropdown-tags', 'placeholder' => 'Selecione ou digite e aperte [Enter]']) !!}
</div>

<div class="well">
    {!! Form::label('ano', 'Ano') !!}
    <div class="form-inline">
        <div class="form-group">
            {!! Form::text('ano_inicial', null, ['class' => 'form-control', 'maxlength' => 4]) !!}
        </div>
        <span style="margin: 0 10px">a</span>
        <div class="form-group">
            {!! Form::text('ano_final', null, ['class' => 'form-control', 'maxlength' => 4]) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('posicao', 'Posição') !!}
    {!! Form::text('posicao', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('dhm', 'DHM') !!}
    {!! Form::text('dhm', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('lado', 'Lado') !!}
    {!! Form::text('lado', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.veiculos.index', $produto->id) }}" class="btn btn-default btn-voltar">Voltar</a>
