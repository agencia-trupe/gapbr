@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('produto_id', 'Produto') !!}
    {!! Form::select('produto_id', $produtos, null, ['class' => 'form-control dropdown-filterable', 'placeholder' => 'Selecione']) !!}
</div>

<div class="form-group">
    {!! Form::label('codigo_concorrente', 'Código do Concorrente') !!}
    {!! Form::text('codigo_concorrente', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.produtos.equivalencia.index') }}" class="btn btn-default btn-voltar">Voltar</a>
