@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            <small>Produtos /</small> Tabela de Equivalência

            <a href="{{ route('painel.produtos.equivalencia.create') }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Equivalência</a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="produtos_categorias">
        <thead>
            <tr>
                <th>Produto</th>
                <th>Código do Concorrente</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <small>{{ $registro->produto->categoria->titulo }}</small>
                    <br><strong>{{ $registro->produto->titulo }}</strong><br>
                    <small>
                        código GAP: {{ $registro->produto->codigo_gap }}
                        |
                        número original: {{ str_replace(['<br>', '<br/>', '<br />'], [',', ',', ','], $registro->produto->numero_original) }}
                    </small>
                </td>
                <td>{{ $registro->codigo_concorrente }}</td>
                <td class="crud-actions">
                    {!! Form::open(array('route' => array('painel.produtos.equivalencia.destroy', $registro->id), 'method' => 'delete')) !!}

                    <div class="btn-group btn-group-sm">
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $registros->render() !!}
    @endif

@endsection
