@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Produtos / Tabela de Equivalência /</small> Adicionar Equivalência</h2>
    </legend>

    {!! Form::open([
        'route' => 'painel.produtos.equivalencia.store',
        'files' => true
    ]) !!}

        @include('painel.produtos.equivalencia.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
