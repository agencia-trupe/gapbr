@extends('painel.common.template')

@section('content')

    <legend>
        <h2>
            Quem Somos
            <a href="{{ route('painel.quem-somos.textos.index') }}" class="btn btn-sm btn-info pull-right">
                <span class="glyphicon glyphicon-align-left" style="margin-right:10px"></span>
                Editar Textos
            </a>
        </h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.quem-somos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.quem-somos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
