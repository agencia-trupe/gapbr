@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Quem Somos / Textos /</small> Editar Texto</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.quem-somos.textos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.quem-somos.textos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
