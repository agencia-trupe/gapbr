@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Quem Somos / Textos /</small> Adicionar Texto</h2>
    </legend>

    {!! Form::open(['route' => 'painel.quem-somos.textos.store', 'files' => true]) !!}

        @include('painel.quem-somos.textos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
