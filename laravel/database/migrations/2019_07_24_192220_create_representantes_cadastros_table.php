<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepresentantesCadastrosTable extends Migration
{
    public function up()
    {
        Schema::create('representantes_cadastros', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('representante_id')->unsigned()->nullable();
            $table->foreign('representante_id')->references('id')->on('representantes')->onDelete('cascade');
            $table->string('nome');
            $table->string('telefones');
            $table->string('e_mail');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('representantes_cadastros');
    }
}
