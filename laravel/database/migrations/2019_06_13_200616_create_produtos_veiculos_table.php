<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdutosVeiculosTable extends Migration
{
    public function up()
    {
        Schema::create('produtos_veiculos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->integer('produto_id')->unsigned()->nullable();
            $table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
            $table->string('montadora');
            $table->string('veiculo');
            $table->string('ano_inicial', 4);
            $table->string('ano_final', 4);
            $table->string('posicao');
            $table->string('dhm');
            $table->string('lado');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('produtos_veiculos');
    }
}
