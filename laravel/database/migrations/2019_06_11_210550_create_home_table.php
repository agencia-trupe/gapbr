<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('chamada_1');
            $table->string('chamada_2');
            $table->string('chamada_3');
            $table->string('chamada_4');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('home');
    }
}
