<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('background');
            $table->text('texto');
            $table->string('botao');
            $table->string('link');
            $table->string('cor');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('banners');
    }
}
