<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatalogoPdfTable extends Migration
{
    public function up()
    {
        Schema::create('catalogo_pdf', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->string('arquivo');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('catalogo_pdf');
    }
}
