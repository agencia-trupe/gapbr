<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    public function run()
    {
        DB::table('home')->insert([
            'imagem' => '',
            'chamada_1' => '',
            'chamada_2' => '',
            'chamada_3' => '',
            'chamada_4' => '',
        ]);
    }
}
