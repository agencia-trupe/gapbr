<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email' => 'contato@trupe.net',
            'telefone' => '11 2695 8487',
            'endereco' => 'Alameda Rubião Júnior, 73 - Mooca<br />São Paulo SP - 03110-030',
            'atendimento' => '8h às 18h, de segunda a sexta-feira',
            'facebook' => 'https://facebook.com',
            'instagram' => 'https://instagram.com'
        ]);
    }
}
