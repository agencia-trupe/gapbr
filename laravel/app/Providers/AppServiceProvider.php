<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view) {
            $view->with('config', \App\Models\Configuracoes::first());
        });

        view()->composer('frontend.common.template', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer('frontend.common.busca-avancada', function($view) {
            $montadoras = \App\Models\ProdutoVeiculo::orderBy('montadora', 'ASC')->pluck('montadora')->unique()->toArray();
            $veiculos = \App\Models\ProdutoVeiculo::orderBy('veiculo', 'ASC')->get()->unique('veiculo');
            $categorias = \App\Models\ProdutoCategoria::ordenados()->lists('titulo', 'id');
            $veiculosAll = \App\Models\ProdutoVeiculo::all();
            $anoMin = $veiculosAll->min('ano_inicial');
            $anoMax = $veiculosAll->max('ano_final');
            $anos = ($anoMin && $anoMax) ? range($anoMin, $anoMax) : [];

            $view->with('buscaMarcas', $montadoras);
            $view->with('buscaModelos', $veiculos);
            $view->with('buscaLinhas', $categorias);
            $view->with('buscaAnos', $anos);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
