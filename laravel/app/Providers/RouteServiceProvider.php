<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('cadastros', 'App\Models\RepresentanteCadastro');
        $router->model('quem-somos_textos', 'App\Models\QuemSomosTexto');
		$router->model('quem-somos', 'App\Models\QuemSomos');
		$router->model('equivalencia', 'App\Models\ProdutoEquivalencia');
		$router->model('produtos', 'App\Models\Produto');
		$router->model('veiculos', 'App\Models\ProdutoVeiculo');
		$router->model('categorias_produtos', 'App\Models\ProdutoCategoria');
		$router->model('produtos-texto', 'App\Models\ProdutosTexto');
		$router->model('novidades', 'App\Models\Novidade');
		$router->model('categorias_novidades', 'App\Models\NovidadeCategoria');
		$router->model('home', 'App\Models\Home');
		$router->model('banners', 'App\Models\Banner');
		$router->model('catalogo-pdf', 'App\Models\CatalogoPdf');
		$router->model('certificacoes', 'App\Models\Certificacao');
		$router->model('qualidade', 'App\Models\Qualidade');
		$router->model('representantes', 'App\Models\Representante');
		$router->model('configuracoes', 'App\Models\Configuracoes');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('produto', function($value) {
            return \App\Models\Produto::whereSlug($value)
                ->firstOrFail();
        });
        $router->bind('produtos_categoria', function($value) {
            return \App\Models\ProdutoCategoria::whereSlug($value)
                ->firstOrFail();
        });

        $router->bind('novidade', function($value) {
            return \App\Models\Novidade::publicados()->whereSlug($value)->firstOrFail();
        });
        $router->bind('novidades_categoria', function($value) {
            return \App\Models\NovidadeCategoria::whereSlug($value)->firstOrFail();
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
