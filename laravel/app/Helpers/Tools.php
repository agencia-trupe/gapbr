<?php

namespace App\Helpers;

class Tools
{
    public static function routeIs($routeNames)
    {
        foreach ((array) $routeNames as $routeName) {
            if (str_is($routeName, \Route::currentRouteName())) {
                return true;
            }
        }

        return false;
    }

    public static function fileUpload($input, $path = 'arquivos/')
    {
        if (! $file = request()->file($input)) {
            throw new \Exception("O campo (${input}) não contém nenhum arquivo.", 1);
        }

        $fileName  = str_slug(
            pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME)
        );
        $fileName .= '_'.date('YmdHis');
        $fileName .= str_random(10);
        $fileName .= '.'.$file->getClientOriginalExtension();

        $file->move(public_path($path), $fileName);

        return $fileName;
    }

    public static function parseLink($url)
    {
        return parse_url($url, PHP_URL_SCHEME) === null ? 'http://'.$url : $url;
    }

    public static function listaEstados()
    {
        $estados = [
            'AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'
        ];
        return array_combine($estados, $estados);
    }

    public static function segmentosNewsletter()
    {
        $segmentos = [
            'distribuidor',
            'varejo',
            'oficina mecânica',
            'consumidor final',
            'outros'
        ];

        return array_combine($segmentos, $segmentos);
    }

    public static function formataData($data)
    {
        $meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

        list($dia, $mes, $ano) = explode('/', $data);

        return $dia . ' ' . substr(strtoupper($meses[(int) $mes - 1]), 0, 3) . ' ' . $ano;
    }

    public static function isEmptyRow($row)
    {
        foreach($row as $cell) {
            if (null !== $cell) return false;
        }

        return true;
    }
}
