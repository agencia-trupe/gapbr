<?php

namespace App\Http\Controllers;

use App\Models\Novidade;
use App\Models\NovidadeCategoria;

class NovidadesController extends Controller
{
    public function __construct()
    {
        view()->share('categorias', NovidadeCategoria::ordenados()->get());
    }

    public function index(NovidadeCategoria $categoria)
    {
        if (request()->get('busca')) {
            $posts = Novidade::busca(request()->get('busca'))->ordenados()->publicados()->paginate(14);
        } elseif ($categoria->exists) {
            $posts = $categoria->novidades()->publicados()->paginate(14);
        } else {
            $posts = Novidade::ordenados()->publicados()->paginate(14);
        }

        if (request()->get('page') && !request()->ajax()) abort('404');

        if (request()->ajax()) {
            return [
                'posts'    => view()->make('frontend.novidades._posts', compact('posts'))->render(),
                'nextPage' => $posts->hasMorePages() ? route('novidades', [
                    null,
                    'page'  => $posts->currentPage() + 1,
                    'busca' => request()->get('busca')
                ]) : false
            ];
        }

        return view('frontend.novidades.index', compact('categoria', 'posts'));
    }

    public function show(NovidadeCategoria $categoria, Novidade $post)
    {
        $leiaMais = Novidade::ordenados()->where('id', '!=', $post->id)->take(3)->get();

        view()->share('novidadesPost', $post);

        return view('frontend.novidades.show', compact('categoria', 'post', 'leiaMais'));
    }
}
