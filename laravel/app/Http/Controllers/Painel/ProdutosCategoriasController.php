<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutoCategoria;

class ProdutosCategoriasController extends Controller
{
    public function index()
    {
        $categorias = ProdutoCategoria::ordenados()->get();

        return view('painel.produtos.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.produtos.categorias.create');
    }

    public function store(ProdutosCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProdutoCategoria::upload_imagem();
            if (isset($input['imagem_home'])) $input['imagem_home'] = ProdutoCategoria::upload_imagem_home();

            ProdutoCategoria::create($input);
            return redirect()->route('painel.produtos.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(ProdutoCategoria $categoria)
    {
        return view('painel.produtos.categorias.edit', compact('categoria'));
    }

    public function update(ProdutosCategoriasRequest $request, ProdutoCategoria $categoria)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = ProdutoCategoria::upload_imagem();
            if (isset($input['imagem_home'])) $input['imagem_home'] = ProdutoCategoria::upload_imagem_home();

            $categoria->update($input);
            return redirect()->route('painel.produtos.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.produtos.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
