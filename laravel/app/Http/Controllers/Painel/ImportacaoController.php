<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Maatwebsite\Excel\Facades\Excel;
use App\Helpers\Tools;

use App\Models\ProdutoCategoria;
use App\Models\Produto;

class ImportacaoController extends Controller
{
    public function index()
    {
        return view('painel.importacao.index');
    }

    public function produtos(Request $request)
    {
        if (! $request->hasFile('arquivo') || strtolower($request->arquivo->getClientOriginalExtension()) != 'xls') {
            return back()->withErrors(['Erro ao importar produtos: Arquivo inválido.']);
        }

        $results = Excel::load($request->arquivo)->get();
        $erros   = [];

        $categorias = [];
        foreach (ProdutoCategoria::all() as $categoria) {
            $categorias[$categoria->slug] = $categoria->id;
        }

        if (get_class($results) == 'Maatwebsite\Excel\Collections\SheetCollection') {
            return back()->withErrors([
                'Erro ao importar produtos: O arquivo deve ter somente uma tabela.'
            ]);
        }

        foreach ($results as $rowNum => $row) {
            $rowNum = $rowNum+2;

            if (Tools::isEmptyRow($row)) continue;
            if (! $row->categoria || $row->categoria == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [categoria] não preenchido.';
            } else {
                $categoria = trim($row->categoria);
                if (! array_key_exists($categoria, $categorias)) {
                    $erros[] = 'Linha '. $rowNum .' - A categoria <strong>'. $categoria .'</strong> não existe. Cadastre-a no painel.';
                }
            }
            if (! $row->titulo || $row->titulo == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [título] não preenchido.';
            }
            if (! $row->codigo_gap || $row->codigo_gap == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [código GAP] não preenchido.';
            }
            if (! $row->numero_original || $row->numero_original == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [número original] não preenchido.';
            }
            if (! $row->dimensoes || $row->dimensoes == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [dimensões] não preenchido.';
            }
        }

        if (count($erros)) {
            return back()->withErrors(
                array_merge(['Erro ao importar arquivo de produtos:'],$erros
            ));
        } else {
            foreach ($results as $row) {
                if (Tools::isEmptyRow($row)) continue;
                Produto::create([
                    'produtos_categoria_id' => $categorias[$categoria],
                    'titulo'                => $row->titulo,
                    'codigo_gap'            => $row->codigo_gap,
                    'numero_original'       => nl2br($row->numero_original),
                    'dimensoes'             => nl2br($row->dimensoes),
                    'observacoes'           => $row->observacoes ? nl2br($row->observacoes) : ''
                ]);
            }
            return back()->with('success', 'Importação realizada com sucesso:  '. count($results) . ' produtos importados.');
        }
    }

    public function veiculos(Request $request)
    {
        if (! $request->hasFile('arquivo') || strtolower($request->arquivo->getClientOriginalExtension()) != 'xls') {
            return back()->withErrors(['Erro ao importar veículos: Arquivo inválido.']);
        }

        $results = Excel::load($request->arquivo)->get();
        $erros   = [];

        $produtos = Produto::all();

        if (get_class($results) == 'Maatwebsite\Excel\Collections\SheetCollection') {
            return back()->withErrors([
                'Erro ao importar veículos: O arquivo deve ter somente uma tabela.'
            ]);
        }

        foreach ($results as $rowNum => $row) {
            $rowNum = $rowNum+2;

            if (Tools::isEmptyRow($row)) continue;
            if (! $row->produto || $row->produto == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [produto] não preenchido.';
            } else {
                $produtoTitulo = trim($row->produto);
                if (! count($produtos->where('titulo', $produtoTitulo))) {
                    $erros[] = 'Linha '. $rowNum .' - O produto <strong>'. $produtoTitulo .'</strong> não existe. Cadastre-o no painel.';
                }
            }
            if (! $row->montadora || $row->montadora == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [montadora] não preenchido.';
            }
            if (! $row->veiculo || $row->veiculo == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [veiculo] não preenchido.';
            }
            if (! $row->ano || $row->ano == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [ano] não preenchido.';
            } else {
                if (preg_match('/^\d{4}-\d{4}$/', $row->ano)) {
                    list($anoInicial, $anoFinal) = explode('-', $row->ano);
                    if ((int)$anoFinal < (int)$anoInicial) {
                        $erros[] = 'Linha '. $rowNum .' - Campo [ano] inválido, o ano final não pode ser menor que o ano inicial.';
                    }
                } else if (! preg_match('/^\d{4}$/', $row->ano)) {
                    $erros[] = 'Linha '. $rowNum .' - Campo [ano] inválido.';
                }
            }
        }

        if (count($erros)) {
            return back()->withErrors(
                array_merge(['Erro ao importar arquivo de veículos:'],$erros
            ));
        } else {
            $count = 0;
            foreach ($results as $row) {
                if (Tools::isEmptyRow($row)) continue;

                if (preg_match('/^\d{4}-\d{4}$/', $row->ano)) {
                    list($anoInicial, $anoFinal) = explode('-', $row->ano);
                } else {
                    $anoInicial = $row->ano;
                    $anoFinal = '';
                }

                foreach($produtos->where('titulo', trim($row->produto)) as $produto) {
                    $produto->veiculos()->create([
                        'montadora'   => $row->montadora,
                        'veiculo'     => $row->veiculo,
                        'ano_inicial' => $anoInicial,
                        'ano_final'   => $anoFinal,
                        'posicao'     => $row->posicao ?: '',
                        'dhm'         => $row->dhm ?: '',
                        'lado'        => $row->lado ?: ''
                    ]);
                    $count++;
                }
            }
            return back()->with('success', 'Importação realizada com sucesso:  '. $count . ' veículos importados.');
        }
    }

    public function equivalencia(Request $request)
    {
        if (! $request->hasFile('arquivo') || strtolower($request->arquivo->getClientOriginalExtension()) != 'xls') {
            return back()->withErrors(['Erro ao importar equivalências: Arquivo inválido.']);
        }

        $results = Excel::load($request->arquivo, function ($reader) {
            $reader->noHeading = true;
        })->get();
        $erros   = [];

        $produtos = Produto::all();

        if (get_class($results) == 'Maatwebsite\Excel\Collections\SheetCollection') {
            return back()->withErrors([
                'Erro ao importar equivalências: O arquivo deve ter somente uma tabela.'
            ]);
        }

        foreach ($results as $rowNum => $row) {
            $rowNum = $rowNum+2;

            if (Tools::isEmptyRow($row)) continue;
            if (! $row[0] || $row[0] == '') {
                $erros[] = 'Linha '. $rowNum .' - Campo [produto] não preenchido.';
            } else {
                $produtoTitulo = trim($row[0]);
                if (! count($produtos->where('titulo', $produtoTitulo))) {
                    $erros[] = 'Linha '. $rowNum .' - O produto <strong>'. $produtoTitulo .'</strong> não existe. Cadastre-o no painel.';
                }
            }
        }

        if (count($erros)) {
            return back()->withErrors(
                array_merge(['Erro ao importar arquivo de equivalências:'],$erros
            ));
        } else {
            $count = 0;
            foreach ($results as $row) {
                if (Tools::isEmptyRow($row)) continue;
                foreach($produtos->where('titulo', trim($row[0])) as $produto) {
                    for($i = 1; $i < count($row); $i++) {
                        if ($row[$i]) {
                            $produto->equivalencias()->create([
                                'codigo_concorrente' => $row[$i]
                            ]);
                            $count++;
                        }
                    }
                }
            }
            return back()->with('success', 'Importação realizada com sucesso:  '. $count . ' equivalências importadas.');
        }
    }

    public function imagens()
    {
        $erros = [];
        $produtos = Produto::where('imagem', '')->get();

        foreach($produtos as $produto) {
            $imagem = str_replace(' ', '-', $produto->titulo).'.png';

            if (! file_exists(public_path('assets/img/produtos/'.$imagem))) {
                $erros[] = 'Imagem <strong>produtos/'.$imagem.'</strong> não encontrada.';
            }
            if (! file_exists(public_path('assets/img/produtos/thumbs/'.$imagem))) {
                $erros[] = 'Imagem <strong>produtos/thumbs/'.$imagem.'</strong> não encontrada.';
            }
        }

        if (count($erros)) {
            return back()->withErrors(
                array_merge(['Erro ao importar imagens:'],$erros
            ));
        }

        $count = 0;
        foreach($produtos as $produto) {
            $imagem = str_replace(' ', '-', $produto->titulo).'.png';
            $produto->update([
                'imagem' => $imagem
            ]);
            $count++;
        }

        return back()->with('success', 'Importação realizada com sucesso:  '. $count . ' imagens importadas.');
    }
}
