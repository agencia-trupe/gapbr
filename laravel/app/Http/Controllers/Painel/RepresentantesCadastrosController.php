<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\RepresentantesCadastrosRequest;
use App\Http\Controllers\Controller;

use App\Models\Representante;
use App\Models\RepresentanteCadastro;

class RepresentantesCadastrosController extends Controller
{
    public function index(Representante $representante)
    {
        $registros = $representante->cadastros;

        return view('painel.representantes.cadastros.index', compact('representante', 'registros'));
    }

    public function create(Representante $representante)
    {
        return view('painel.representantes.cadastros.create', compact('representante'));
    }

    public function store(RepresentantesCadastrosRequest $request, Representante $representante)
    {
        try {

            $input = $request->all();

            $representante->cadastros()->create($input);

            return redirect()->route('painel.representantes.cadastros.index', compact('representante'))->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Representante $representante, RepresentanteCadastro $registro)
    {
        return view('painel.representantes.cadastros.edit', compact('representante', 'registro'));
    }

    public function update(RepresentantesCadastrosRequest $request, Representante $representante, RepresentanteCadastro $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.representantes.cadastros.index', $representante->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Representante $representante, RepresentanteCadastro $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.representantes.cadastros.index', $representante->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
