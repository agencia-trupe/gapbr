<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\QuemSomosTextosRequest;
use App\Http\Controllers\Controller;

use App\Models\QuemSomosTexto;

class QuemSomosTextosController extends Controller
{
    public function index()
    {
        $registros = QuemSomosTexto::ordenados()->get();

        return view('painel.quem-somos.textos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.quem-somos.textos.create');
    }

    public function store(QuemSomosTextosRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = QuemSomosTexto::upload_imagem();

            QuemSomosTexto::create($input);

            return redirect()->route('painel.quem-somos.textos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(QuemSomosTexto $registro)
    {
        return view('painel.quem-somos.textos.edit', compact('registro'));
    }

    public function update(QuemSomosTextosRequest $request, QuemSomosTexto $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = QuemSomosTexto::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.quem-somos.textos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(QuemSomosTexto $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.quem-somos.textos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
