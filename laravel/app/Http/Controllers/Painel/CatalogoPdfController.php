<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CatalogoPdfRequest;
use App\Http\Controllers\Controller;

use App\Models\CatalogoPdf;
use App\Helpers\Tools;

class CatalogoPdfController extends Controller
{
    public function index()
    {
        $registro = CatalogoPdf::first();

        return view('painel.catalogo-pdf.edit', compact('registro'));
    }

    public function update(CatalogoPdfRequest $request, CatalogoPdf $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['arquivo'])) $input['arquivo'] = Tools::fileUpload('arquivo', 'assets/catalogo-pdf/');

            $registro->update($input);

            return redirect()->route('painel.catalogo-pdf.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
