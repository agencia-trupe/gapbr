<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\NovidadesCategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\NovidadeCategoria;

class NovidadesCategoriasController extends Controller
{
    public function index()
    {
        $categorias = NovidadeCategoria::ordenados()->get();

        return view('painel.novidades.categorias.index', compact('categorias'));
    }

    public function create()
    {
        return view('painel.novidades.categorias.create');
    }

    public function store(NovidadesCategoriasRequest $request)
    {
        try {

            $input = $request->all();

            NovidadeCategoria::create($input);
            return redirect()->route('painel.novidades.categorias.index')->with('success', 'Categoria adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar categoria: '.$e->getMessage()]);

        }
    }

    public function edit(NovidadeCategoria $categoria)
    {
        return view('painel.novidades.categorias.edit', compact('categoria'));
    }

    public function update(NovidadesCategoriasRequest $request, NovidadeCategoria $categoria)
    {
        try {

            $input = $request->all();

            $categoria->update($input);
            return redirect()->route('painel.novidades.categorias.index')->with('success', 'Categoria alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar categoria: '.$e->getMessage()]);

        }
    }

    public function destroy(NovidadeCategoria $categoria)
    {
        try {

            $categoria->delete();
            return redirect()->route('painel.novidades.categorias.index')->with('success', 'Categoria excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir categoria: '.$e->getMessage()]);

        }
    }
}
