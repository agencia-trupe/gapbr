<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\QualidadeRequest;
use App\Http\Controllers\Controller;

use App\Models\Qualidade;
use App\Helpers\Tools;

class QualidadeController extends Controller
{
    public function index()
    {
        $registro = Qualidade::first();

        return view('painel.qualidade.edit', compact('registro'));
    }

    public function update(QualidadeRequest $request, Qualidade $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Qualidade::upload_imagem();
            if (isset($input['manual_capa'])) $input['manual_capa'] = Qualidade::upload_manual_capa();
            if (isset($input['manual_arquivo'])) $input['manual_arquivo'] = Tools::fileUpload('manual_arquivo', 'assets/manual-de-qualidade/');

            $registro->update($input);

            return redirect()->route('painel.qualidade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
