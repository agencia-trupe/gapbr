<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosVeiculosRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoVeiculo;

class ProdutosVeiculosController extends Controller
{
    public function index(Produto $produto)
    {
        $registros = $produto->veiculos;

        return view('painel.produtos.veiculos.index', compact('produto', 'registros'));
    }

    public function create(Produto $produto, Request $request)
    {
        $montadoras = ProdutoVeiculo::orderBy('montadora', 'ASC')
            ->pluck('montadora')->unique()->toArray();
        if ($request->old('montadora') && ! array_key_exists($request->old('montadora'), $montadoras)) {
            $montadoras[] = $request->old('montadora');
        }
        $montadoras = array_combine($montadoras, $montadoras);

        $veiculos = ProdutoVeiculo::orderBy('veiculo', 'ASC')
            ->pluck('veiculo')->unique()->toArray();
        if ($request->old('veiculo') && ! array_key_exists($request->old('veiculo'), $veiculos)) {
            $veiculos[] = $request->old('veiculo');
        }
        $veiculos = array_combine($veiculos, $veiculos);

        return view('painel.produtos.veiculos.create', compact('produto', 'montadoras', 'veiculos'));
    }

    public function store(Produto $produto, ProdutosVeiculosRequest $request)
    {
        try {

            if ($request->ano_final && $request->ano_final < $request->ano_inicial) {
                throw new \Exception('O ano final não pode ser menor que o ano inicial.');
            }

            $produto->veiculos()->create($request->all());

            return redirect()->route('painel.produtos.veiculos.index', $produto->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()])->withInput();

        }
    }

    public function edit(Produto $produto, ProdutoVeiculo $registro, Request $request)
    {
        $montadoras = ProdutoVeiculo::orderBy('montadora', 'ASC')
            ->pluck('montadora')->unique()->toArray();
        if ($request->old('montadora') && ! array_key_exists($request->old('montadora'), $montadoras)) {
            $montadoras[] = $request->old('montadora');
        }
        $montadoras = array_combine($montadoras, $montadoras);

        $veiculos = ProdutoVeiculo::orderBy('veiculo', 'ASC')
            ->pluck('veiculo')->unique()->toArray();
        if ($request->old('veiculo') && ! array_key_exists($request->old('veiculo'), $veiculos)) {
            $veiculos[] = $request->old('veiculo');
        }
        $veiculos = array_combine($veiculos, $veiculos);

        return view('painel.produtos.veiculos.edit', compact('produto', 'registro', 'montadoras', 'veiculos'));
    }

    public function destroy(Produto $produto, ProdutoVeiculo $registro)
    {
        try {

            if (! $produto->veiculos->contains($registro)) {
                throw new \Exception('Veículo inválido');
            }

            $registro->delete();

            return redirect()->route('painel.produtos.veiculos.index', $produto->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

    public function update(Produto $produto, ProdutoVeiculo $registro, ProdutosVeiculosRequest $request)
    {
        try {

            if ($request->ano_final && $request->ano_final < $request->ano_inicial) {
                throw new \Exception('O ano final não pode ser menor que o ano inicial.');
            }

            $registro->update($request->all());

            return redirect()->route('painel.produtos.veiculos.index', $produto->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()])->withInput();

        }
    }
}
