<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProdutosTextoRequest;
use App\Http\Controllers\Controller;

use App\Models\ProdutosTexto;

class ProdutosTextoController extends Controller
{
    public function index()
    {
        $registro = ProdutosTexto::first();

        return view('painel.produtos-texto.edit', compact('registro'));
    }

    public function update(ProdutosTextoRequest $request, ProdutosTexto $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.produtos-texto.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
