<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CertificacoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Certificacao;

class CertificacoesController extends Controller
{
    public function index()
    {
        $registros = Certificacao::ordenados()->get();

        return view('painel.certificacoes.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.certificacoes.create');
    }

    public function store(CertificacoesRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Certificacao::upload_imagem();

            Certificacao::create($input);

            return redirect()->route('painel.certificacoes.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Certificacao $registro)
    {
        return view('painel.certificacoes.edit', compact('registro'));
    }

    public function update(CertificacoesRequest $request, Certificacao $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Certificacao::upload_imagem();

            $registro->update($input);

            return redirect()->route('painel.certificacoes.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Certificacao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.certificacoes.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
