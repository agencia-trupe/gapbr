<?php

namespace App\Http\Controllers\Painel;

use App\Http\Requests\ProdutosEquivalenciaRequest;
use App\Http\Controllers\Controller;

use App\Models\Produto;
use App\Models\ProdutoEquivalencia;

class ProdutosEquivalenciaController extends Controller
{
    public function index()
    {
        $registros = ProdutoEquivalencia::
            join('produtos', 'produto_id', '=', 'produtos.id')
            ->orderBy('produtos.titulo', 'ASC')
            ->select('produtos_equivalencia.*')
            ->paginate(15);

        return view('painel.produtos.equivalencia.index', compact('registros'));
    }

    public function create()
    {
        $produtos = Produto::leftJoin('produtos_categorias as cat', 'cat.id', '=', 'produtos_categoria_id')
            ->orderBy('cat.ordem', 'ASC')
            ->orderBy('cat.id', 'DESC')
            ->select('produtos.*')
            ->ordenados()->get()
            ->lists('titulo_painel', 'id');

        return view('painel.produtos.equivalencia.create', compact('produtos'));
    }

    public function store(ProdutosEquivalenciaRequest $request)
    {
        try {

            $produto = Produto::findOrFail($request->produto_id);

            if ($produto->equivalencias->contains('codigo_concorrente', $request->codigo_concorrente)) {
                throw new \Exception('A equivalência inserida já existe.');
            }

            $produto->equivalencias()->create([
                'codigo_concorrente' => $request->codigo_concorrente
            ]);

            return redirect()->route('painel.produtos.equivalencia.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(ProdutoEquivalencia $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.produtos.equivalencia.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
