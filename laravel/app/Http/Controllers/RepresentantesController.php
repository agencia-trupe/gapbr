<?php

namespace App\Http\Controllers;

use App\Models\Representante;

class RepresentantesController extends Controller
{
    public function index()
    {
        $estados = Representante::orderBy('estado', 'ASC')
            ->pluck('estado')->toArray();

        if (request('uf')) {
            $uf      = request('uf');
            $cidade  = request('cidade');

            $cidades = Representante::where('estado', $uf)
                ->orderBy('cidade', 'ASC')
                ->pluck('cidade', 'cidade')
                ->toArray();

            $representantes = Representante::where('estado', $uf);

            if ($cidade && in_array($cidade, $cidades)) {
                $representantes = $representantes->where('cidade', $cidade);
            }

            $representantes = $representantes->with('cadastros')->ordenados()->get();

            return view('frontend.representantes', compact('estados', 'cidades', 'uf', 'cidade', 'representantes'));
        }

        return view('frontend.representantes', compact('estados'));
    }
}
