<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\CatalogoPdf;
use App\Models\Produto;

class CatalogoController extends Controller
{
    public function index(Request $request)
    {
        if ($busca = $this->isBusca($request)) {
            $produtos = Produto::orderBy('titulo', 'ASC');

            if ($request->codigo_gap) {
                $produtos = $produtos->where('codigo_gap', 'LIKE', "%{$request->codigo_gap}%");
            }
            if ($request->numero_original) {
                $produtos = $produtos->where('numero_original', 'LIKE', "%{$request->numero_original}%");
            }
            if ($request->equivalencia) {
                $produtos = $produtos->whereHas('equivalencias', function($query) use ($request) {
                    $query->where('codigo_concorrente', 'LIKE', "%{$request->equivalencia}%");
                });
            }
            if ($request->marca) {
                $produtos = $produtos->whereHas('veiculos', function($query) use ($request) {
                    $query->where('montadora', $request->marca);
                });
            }
            if ($request->modelo) {
                $produtos = $produtos->whereHas('veiculos', function($query) use ($request) {
                    $query->where('veiculo', $request->modelo);
                });
            }
            if ($request->ano) {
                $produtos = $produtos->whereHas('veiculos', function($query) use ($request) {
                    $query->whereRaw('? between ano_inicial and ano_final', [$request->ano]);
                });
            }
            if ($request->linha) {
                $produtos = $produtos->where('produtos_categoria_id', (int)$request->linha);
            }

            $produtos = $produtos->get();

            return view('frontend.catalogo.index', compact('busca', 'produtos'));
        }

        $produtos = Produto::orderBy('titulo', 'ASC')->paginate(20);

        return view('frontend.catalogo.index', compact('produtos'));
    }

    private function isBusca(Request $request)
    {
        foreach([
            'codigo_gap', 'numero_original', 'equivalencia',
            'marca', 'modelo', 'ano', 'linha'
        ] as $field) {
            if ($request->has($field)) return true;
        }

        return false;
    }

    public function show(Produto $produto)
    {
        return view('frontend.catalogo.show', compact('produto'));
    }

    public function pdf()
    {
        $catalogoPdf = CatalogoPdf::first();

        return view('frontend.catalogo.pdf', compact('catalogoPdf'));
    }
}
