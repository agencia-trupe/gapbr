<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsletterRequest;

use App\Models\Banner;
use App\Models\ProdutoCategoria;
use App\Models\Home;
use App\Models\Novidade;
use App\Models\Newsletter;

class HomeController extends Controller
{
    public function index()
    {
        $banners    = Banner::ordenados()->get();
        $categorias = ProdutoCategoria::ordenados()->get();
        $home       = Home::first();
        $novidades  = Novidade::publicados()->ordenados()->take(2)->get();

        return view('frontend.home', compact('banners', 'categorias', 'home', 'novidades'));
    }

    public function newsletter(NewsletterRequest $request)
    {
        Newsletter::create($request->all());

        return response()->json([
            'message' => 'Cadastro efetuado com sucesso!'
        ]);
    }
}
