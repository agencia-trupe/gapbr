<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\QuemSomos;
use App\Models\QuemSomosTexto;
use App\Models\ProdutosTexto;
use App\Models\ProdutoCategoria;
use App\Models\Novidade;
use App\Models\Qualidade;
use App\Models\Representante;
use App\Models\Contato;

class BuscaController extends Controller
{
    public function index(Request $request)
    {
        $termo = $request->palavra_chave;
        if (! $termo) return redirect()->route('home');

        $resultados = [];

        // quem somos
        if (QuemSomos::where('texto', 'LIKE', "%$termo%")->first()) {
            $resultados[] = [
                'secao' => null,
                'titulo' => 'Quem Somos',
                'link' => route('quemSomos')
            ];
        }

        // quem somos / texto
        foreach(QuemSomosTexto::where('titulo', 'LIKE', "%$termo%")->orWhere('texto', 'LIKE', "%$termo%")->get() as $quemSomosTexto) {
            $resultados[] = [
                'secao' => 'Quem Somos',
                'titulo' => $quemSomosTexto->titulo,
                'link' => route('quemSomos')
            ];
        }

        // produtos
        if (ProdutosTexto::where('texto', 'LIKE', "%$termo%")->first()) {
            $resultados[] = [
                'secao' => null,
                'titulo' => 'Produtos',
                'link' => route('produtos')
            ];
        }

        // produtos / categoria
        foreach(ProdutoCategoria::where('titulo', 'LIKE', "%$termo%")->orWhere('texto', 'LIKE', "%$termo%")->orWhere('chamada', 'LIKE', "%$termo%")->get() as $produtosCategoria) {
            $resultados[] = [
                'secao' => 'Produtos',
                'titulo' => $produtosCategoria->titulo,
                'link' => route('produtos', $produtosCategoria->slug)
            ];
        }

        // novidades / novidade
        foreach(Novidade::where('titulo', 'LIKE', "%$termo%")->orWhere('texto', 'LIKE', "%$termo%")->get() as $novidade) {
            $resultados[] = [
                'secao' => 'Novidades',
                'titulo' => $novidade->titulo,
                'link' => route('novidades.show', [$novidade->categoria->slug, $novidade->slug])
            ];
        }

        // qualidade
        if (Qualidade::where('texto_1', 'LIKE', "%$termo%")->orWhere('texto_2', 'LIKE', "%$termo%")->first()) {
            $resultados[] = [
                'secao' => null,
                'titulo' => 'Qualidade',
                'link' => route('qualidade')
            ];
        }

        // representantes / cidade-uf
        foreach(Representante::where('estado', 'LIKE', "%$termo%")->orWhere('cidade', 'LIKE', "%$termo%")->orWhere('titulo', 'LIKE', "%$termo%")->orWhere('texto', 'LIKE', "%$termo%")->get() as $representante) {
            $resultados[] = [
                'secao' => 'Representantes',
                'titulo' => "$representante->cidade - $representante->estado",
                'link' => route('representantes', [
                    'uf' => $representante->estado,
                    'cidade' => $representante->cidade
                ])
            ];
        }

        // contato
        if (Contato::where('telefone', 'LIKE', "%$termo%")->orWhere('endereco', 'LIKE', "%$termo%")->orWhere('atendimento', 'LIKE', "%$termo%")->first()) {
            $resultados[] = [
                'secao' => null,
                'titulo' => 'Contato',
                'link' => route('contato')
            ];
        }

        $resultados = collect($resultados)->unique();

        return view('frontend.busca', compact('resultados'));
    }
}
