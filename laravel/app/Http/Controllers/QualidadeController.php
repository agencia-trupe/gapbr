<?php

namespace App\Http\Controllers;

use App\Models\Qualidade;
use App\Models\Certificacao;

class QualidadeController extends Controller
{
    public function index()
    {
        $qualidade = Qualidade::first();
        $certificacoes = Certificacao::ordenados()->get();

        return view('frontend.qualidade', compact('qualidade', 'certificacoes'));
    }
}
