<?php

namespace App\Http\Controllers;

use App\Models\ProdutoCategoria;
use App\Models\ProdutosTexto;

class ProdutosController extends Controller
{
    public function index(ProdutoCategoria $categoria)
    {
        $categorias = ProdutoCategoria::ordenados()->get();

        if ($categoria->exists) {
            return view('frontend.produtos.categoria', compact('categorias', 'categoria'));
        }

        $produtosTexto = ProdutosTexto::first();

        return view('frontend.produtos.index', compact('produtosTexto', 'categorias'));
    }
}
