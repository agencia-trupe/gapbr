<?php

namespace App\Http\Controllers;

use App\Models\QuemSomos;
use App\Models\QuemSomosTexto;

class QuemSomosController extends Controller
{
    public function index()
    {
        $quemSomos = QuemSomos::first();
        $textos    = QuemSomosTexto::ordenados()->get();

        return view('frontend.quem-somos', compact('quemSomos', 'textos'));
    }
}
