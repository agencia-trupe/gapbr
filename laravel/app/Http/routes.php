<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('busca', 'BuscaController@index')->name('busca');
    Route::post('newsletter', 'HomeController@newsletter')->name('newsletter');
    Route::get('quem-somos', 'QuemSomosController@index')->name('quemSomos');
    Route::get('produtos/{produtos_categoria?}', 'ProdutosController@index')->name('produtos');
    Route::get('catalogo', 'CatalogoController@index')->name('catalogo');
    Route::get('catalogo/{produto}', 'CatalogoController@show')->name('catalogo.show');
    Route::get('catalogo-pdf', 'CatalogoController@pdf')->name('catalogoPdf');
    Route::get('novidades/{novidades_categoria?}', 'NovidadesController@index')->name('novidades');
    Route::get('novidades/{novidades_categoria}/{novidade}', 'NovidadesController@show')->name('novidades.show');
    Route::get('qualidade', 'QualidadeController@index')->name('qualidade');
    Route::get('representantes', 'RepresentantesController@index')->name('representantes');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('quem-somos/textos', 'QuemSomosTextosController', ['parameters' => ['textos' => 'quem-somos_textos']]);
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
        Route::resource('produtos/equivalencia', 'ProdutosEquivalenciaController', ['except' => ['edit', 'update']]);
		Route::resource('produtos/categorias', 'ProdutosCategoriasController', ['parameters' => ['categorias' => 'categorias_produtos']]);
		Route::resource('produtos', 'ProdutosController');
		Route::resource('produtos.veiculos', 'ProdutosVeiculosController');
		Route::resource('produtos-texto', 'ProdutosTextoController', ['only' => ['index', 'update']]);
		Route::resource('novidades/categorias', 'NovidadesCategoriasController', ['parameters' => ['categorias' => 'categorias_novidades']]);
		Route::resource('novidades', 'NovidadesController');
		Route::resource('home', 'HomeController', ['only' => ['index', 'update']]);
        Route::get('banners/{banners}/remover-imagem/{key}', 'BannersController@removerImagem')->name('painel.banners.removerImagem');
		Route::resource('banners', 'BannersController');
		Route::resource('catalogo-pdf', 'CatalogoPdfController', ['only' => ['index', 'update']]);
		Route::resource('certificacoes', 'CertificacoesController');
		Route::resource('qualidade', 'QualidadeController', ['only' => ['index', 'update']]);
		Route::resource('representantes', 'RepresentantesController');
		Route::resource('representantes.cadastros', 'RepresentantesCadastrosController');
		Route::resource('configuracoes', 'ConfiguracoesController', ['only' => ['index', 'update']]);

        Route::get('contato/recebidos/{recebidos}/toggle', ['as' => 'painel.contato.recebidos.toggle', 'uses' => 'ContatosRecebidosController@toggle']);
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::resource('newsletter', 'NewsletterController', ['only' => ['index', 'destroy']]);
        Route::get('newsletter/exportar', 'NewsletterController@exportar')->name('painel.newsletter.exportar');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

        Route::get('importacao', 'ImportacaoController@index')->name('painel.importacao');
        Route::post('importacao/produtos', 'ImportacaoController@produtos')->name('painel.importacao.produtos');
        Route::post('importacao/veiculos', 'ImportacaoController@veiculos')->name('painel.importacao.veiculos');
        Route::post('importacao/equivalencia', 'ImportacaoController@equivalencia')->name('painel.importacao.equivalencia');
        Route::get('importacao/imagens', 'ImportacaoController@imagens')->name('painel.importacao.imagens');

        Route::get('generator', 'GeneratorController@index')->name('generator.index');
        Route::post('generator', 'GeneratorController@submit')->name('generator.submit');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
