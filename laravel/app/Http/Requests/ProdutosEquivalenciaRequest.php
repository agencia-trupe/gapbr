<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosEquivalenciaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'produto_id' => 'required',
            'codigo_concorrente' => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'produto_id' => 'produto',
            'codigo_concorrente' => 'código do concorrente'
        ];
    }
}
