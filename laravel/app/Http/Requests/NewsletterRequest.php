<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Helpers\Tools;

class NewsletterRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email|unique:newsletter,email',
            'segmento' => 'required|in:'.join(',', Tools::segmentosNewsletter())
        ];
    }

    public function messages()
    {
        return [
            'nome.required'     => 'Preencha seu nome.',
            'email.required'    => 'Preencha seu e-mail.',
            'email.email'       => 'Insira um e-mail válido.',
            'email.unique'      => 'Este e-mail já está cadastrado.',
            'segmento.required' => 'Selecione um segmento.',
            'segmento.in'       => 'Segmento inválido.'
        ];
    }
}
