<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'produtos_categoria_id' => 'required',
            'titulo' => 'required',
            'codigo_gap' => 'required',
            'numero_original' => 'required',
            'imagem' => 'required|image',
            'dimensoes' => 'required',
            'observacoes' => '',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'produtos_categoria_id' => 'categoria',
            'titulo'                => 'título',
            'codigo_gap'            => 'código GAP',
            'numero_original'       => 'número original',
            'dimensoes'             => 'dimensões',
        ];
    }
}
