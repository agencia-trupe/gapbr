<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProdutosVeiculosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'montadora'   => 'required',
            'veiculo'     => 'required',
            'ano_inicial' => 'required|digits:4',
            'ano_final'   => 'digits:4',
            'posicao'     => '',
            'dhm'         => 'required',
            'lado'        => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'veiculo'     => 'veículo',
            'ano_inicial' => 'ano (inicial)',
            'ano_final'   => 'ano (final)',
            'posicao'     => 'posição'
        ];
    }
}
