<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class QualidadeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_1' => 'required',
            'texto_2' => 'required',
            'imagem' => 'image',
            'manual_arquivo' => '',
            'manual_capa' => 'image',
        ];
    }
}
