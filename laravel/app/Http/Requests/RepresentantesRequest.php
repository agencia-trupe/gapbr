<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RepresentantesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'estado' => 'required',
            'cidade' => 'required',
            'titulo' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
