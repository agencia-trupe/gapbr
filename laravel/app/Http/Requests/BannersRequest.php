<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'background' => 'required|image',
            'cor' => 'required|in:branco,preto',
            'texto' => '',
            'botao' => 'required_with:link',
            'link' => 'required_with:botao',
            'imagem_1' => 'image',
            'imagem_2' => 'image',
            'imagem_3' => 'image',
        ];

        if ($this->method() != 'POST') {
            $rules['background'] = 'image';
            $rules['imagem_1'] = 'image';
            $rules['imagem_2'] = 'image';
            $rules['imagem_3'] = 'image';
        }

        return $rules;
    }
}
