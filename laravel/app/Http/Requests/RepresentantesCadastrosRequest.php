<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RepresentantesCadastrosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'telefones' => 'required',
            'e_mail' => 'required|email',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
