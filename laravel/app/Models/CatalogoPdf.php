<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class CatalogoPdf extends Model
{
    protected $table = 'catalogo_pdf';

    protected $guarded = ['id'];

}
