<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Representante extends Model
{
    protected $table = 'representantes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query
            ->orderBy('estado', 'ASC')
            ->orderBy('cidade', 'ASC')
            ->orderBy('ordem', 'ASC')
            ->orderBy('id', 'DESC');
    }

    public function cadastros()
    {
        return $this->hasMany(RepresentanteCadastro::class, 'representante_id')->ordenados();
    }
}
