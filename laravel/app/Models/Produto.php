<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Produto extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('titulo', 'ASC');
    }

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('produtos_categoria_id', $categoria_id);
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoCategoria', 'produtos_categoria_id');
    }

    public function equivalencias()
    {
        return $this->hasMany(ProdutoEquivalencia::class, 'produto_id');
    }

    public function veiculos()
    {
        return $this->hasMany(ProdutoVeiculo::class, 'produto_id')->ordenados();
    }

    public function getTituloPainelAttribute()
    {
        $titulo  = '';
        $titulo .= $this->categoria ? $this->categoria->titulo : 'sem categoria';
        $titulo .= ' | '.$this->titulo.' - ';
        $titulo .= 'código GAP: '.$this->codigo_gap.' - ';
        $titulo .= 'número original: '.str_replace(['<br>', '<br/>', '<br />'], [',', ',', ','], $this->numero_original);

        return $titulo;
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 500,
                'height' => 500,
                'path'   => 'assets/img/produtos/',
                'upsize' => true,
                'transparent' => true
            ],
            [
                'width'  => 280,
                'height' => 280,
                'path'   => 'assets/img/produtos/thumbs/',
                'upsize' => true,
                'transparent' => true
            ]
        ]);
    }
}
