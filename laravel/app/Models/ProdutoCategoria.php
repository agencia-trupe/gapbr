<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Helpers\CropImage;

class ProdutoCategoria extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'produtos_categorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    public function produtos()
    {
        return $this->hasMany('App\Models\Produto', 'produtos_categoria_id')->ordenados();
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 300,
            'height' => 280,
            'path'   => 'assets/img/produtos/categorias/'
        ]);
    }

    public static function upload_imagem_home()
    {
        return CropImage::make('imagem_home', [
            'width'  => 280,
            'height' => 280,
            'path'   => 'assets/img/produtos/categorias/home/',
            'upsize' => true,
            'transparent' => true
        ]);
    }
}
