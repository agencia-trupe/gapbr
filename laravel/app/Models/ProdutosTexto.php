<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ProdutosTexto extends Model
{
    protected $table = 'produtos_texto';

    protected $guarded = ['id'];

}
