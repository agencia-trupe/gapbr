<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use Carbon\Carbon;

class Novidade extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'novidades';

    protected $guarded = ['id'];

    public function scopeCategoria($query, $categoria_id)
    {
        return $query->where('novidades_categoria_id', $categoria_id);
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('data', 'DESC')->orderBy('id', 'DESC');
    }

    public function scopePublicados($query)
    {
        return $query->whereDate('data', '<=', Carbon::today()->toDateString());
    }

    public function getAgendadoAttribute()
    {
        return Carbon::createFromFormat('d/m/Y', $this->data) > Carbon::now();
    }

    public function scopeBusca($query, $termo)
    {
        return $query
            ->where('titulo', 'LIKE', "%{$termo}%")
            ->orWhere('texto', 'LIKE', "%{$termo}%");
    }

    public function setDataAttribute($date)
    {
        $this->attributes['data'] = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
    }

    public function getDataAttribute($date)
    {
        return Carbon::createFromFormat('Y-m-d', $date)->format('d/m/Y');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Models\NovidadeCategoria', 'novidades_categoria_id');
    }

    public static function upload_capa()
    {
        return CropImage::make('capa', [
            [
                'width'  => 622,
                'height' => 350,
                'path'   => 'assets/img/novidades/'
            ],
            [
                'width'  => 306,
                'height' => 172,
                'path'   => 'assets/img/novidades/thumb-md/'
            ],
            [
                'width'  => 196,
                'height' => 110,
                'path'   => 'assets/img/novidades/thumb-sm/'
            ]
        ]);
    }
}
