<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Certificacao extends Model
{
    protected $table = 'certificacoes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            [
                'width'  => 154,
                'height' => 218,
                'path'   => 'assets/img/certificacoes/'
            ],
            [
                'width'  => 980,
                'height' => null,
                'path'   => 'assets/img/certificacoes/ampliacao/'
            ],
        ]);
    }
}
