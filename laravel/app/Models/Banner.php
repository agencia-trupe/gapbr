<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Banner extends Model
{
    protected $table = 'banners';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function upload_background()
    {
        return CropImage::make('background', [
            'width'  => 1980,
            'height' => null,
            'path'   => 'assets/img/banners/'
        ]);
    }

    public static function upload_imagem_1()
    {
        return CropImage::make('imagem_1', [
            'width'  => 130,
            'height' => 130,
            'path'   => 'assets/img/banners/',
            'transparent' => true
        ]);
    }

    public static function upload_imagem_2()
    {
        return CropImage::make('imagem_2', [
            'width'  => 130,
            'height' => 130,
            'path'   => 'assets/img/banners/',
            'transparent' => true
        ]);
    }

    public static function upload_imagem_3()
    {
        return CropImage::make('imagem_3', [
            'width'  => 130,
            'height' => 130,
            'path'   => 'assets/img/banners/',
            'transparent' => true
        ]);
    }
}
