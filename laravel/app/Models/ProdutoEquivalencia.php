<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoEquivalencia extends Model
{
    protected $table = 'produtos_equivalencia';

    protected $guarded = ['id'];

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id');
    }
}
