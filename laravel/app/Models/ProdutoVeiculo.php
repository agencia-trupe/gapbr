<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoVeiculo extends Model
{
    protected $table = 'produtos_veiculos';

    protected $guarded = ['id'];

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id');
    }

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function getAnoAttribute()
    {
        if ($this->ano_final) {
            return $this->ano_inicial == $this->ano_final
                ? $this->ano_inicial
                : $this->ano_inicial.' a '.$this->ano_final;
        }

        return $this->ano_inicial.' a atual';
    }
}
