<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Qualidade extends Model
{
    protected $table = 'qualidade';

    protected $guarded = ['id'];

    public static function upload_manual_capa()
    {
        return CropImage::make('manual_capa', [
            'width'  => 112,
            'height' => null,
            'path'   => 'assets/img/qualidade/'
        ]);
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 960,
            'height' => 250,
            'path'   => 'assets/img/qualidade/'
        ]);
    }
}
